#include <stdio.h>
#include <getopt.h>
#include <string.h>
#include <stdlib.h>

struct Flags {
    char *fileNameInput;
    char *fileNameOutput; 
    int thresholdScope;
    int halfWhiteThresholdScope;
    int halfBlackThresholdScope;
    int levelCorrectionScope[2];
    int conturingScope;
    double gammaCorectionScope;
    unsigned int red : 1;
    unsigned int green : 1;
    unsigned int blue : 1;
    unsigned int negative : 1;
    unsigned int halfWhiteThreshold : 1;
    unsigned int halfBlackThreshold : 1;
    unsigned int threshold : 1;
    unsigned int gammaCorection : 1;
    unsigned int levelCorrection : 1;
    unsigned int conturing : 1;
    unsigned int horizontalBlur : 1;
    unsigned int verticalBlur : 1;
    unsigned int histogram : 1;
};
typedef struct Flags Flags;

Flags initializeArgumentParser(int argc, char ** argv);
void defineArguments(Flags *input, int argc, char ** argv);
void validateParsedArguments(Flags *input);

int isRed(char c);
int isGreen(char c);
int isBlue(char c);