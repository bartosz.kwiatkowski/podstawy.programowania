#include "PictureIO.h"
#include "Flags.h"
#include <math.h>

void convertPictureUsingFlags(Picture *picture, Flags *flags);

void setNegative(Picture *picture);
void setThreshold(Picture *picture, int thresholdScope);
void setHalfWhiteThreshold(Picture *picture, int halfThresholdScope);
void setHalfBlackThreshold(Picture *picture, int halfBlackThresholdScope);
void setGammaCorrection(Picture *picture, double gamma);
void setLevelCorrection(Picture *picture, int white, int black);
void setConturing(Picture *picture, int scope);
void setHorizontalBlur(Picture *picture);
void setVerticalBlur(Picture *picture);
void setHistogram(Picture *picture);

void setRedLevelCorrection(RGB *rgb, int blackScope, int whiteScope);
int sizeOfPicture(Picture *picture);
