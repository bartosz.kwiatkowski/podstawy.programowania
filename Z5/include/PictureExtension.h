#include <stdint.h>
#include <string.h>
#include <stdio.h>

typedef uint8_t BYTE;

void defineFileFormat(FILE *file, char *output);
char* compareFileExtension(char *extension);