#include "PictureImp.h"

static char P2[] = "P2";
static char P3[] = "P3";
static char JPG[] = "JPG";

Picture initializePicture(char fileName[]);

void readPictureFromFile(FILE *input);
void savePictureToFile(Picture input, char fileName[]);

void readPixelsFromFile(FILE *input);
void savePixelsToFile(FILE *output);
void readPictureSettings(FILE *input);
void savePictureSettings(FILE *output);

bool isExtensionEqualsInput(const char *input);