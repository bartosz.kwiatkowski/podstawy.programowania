#include "RGB.h"

struct Pixel {
    RGB RGBcolor;
};

typedef struct Pixel Pixel;

RGB getPixelColor(Pixel *pixel);
void setPixelColor(Pixel *pixel, RGB rgb);
