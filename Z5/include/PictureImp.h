#include <stdbool.h> 
#include <string.h>
#include "Pixel.h"
#include "PictureExtension.h"

typedef struct Picture Picture;

struct Picture {
    Pixel *pixels;
    int width;
    int height;
    int colorScale;
    char fileExtension[3];
};

void throwError(char message[], int number);