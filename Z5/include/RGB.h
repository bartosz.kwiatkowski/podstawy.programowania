#include <stdio.h>
#include <stdlib.h>

struct RGB {
    unsigned char colorScale[3];
};

typedef struct RGB RGB;

// methods
RGB initByRGB(unsigned char red, unsigned char green, unsigned char blue);
RGB initByShade(short shade);

// setters
void setRedScale(RGB *rgb, unsigned char scale);
void setGreenScale(RGB *rgb, unsigned char scale);
void setBlueScale(RGB *rgb, unsigned char scale);
void setShadeScale(RGB *rgb, int scale);

// Getters
short getRedScale(RGB *rgb);
short getGreenScale(RGB *rgb);
short getBlueScale(RGB *rgb);
short getShadeScale(RGB *rgb);

