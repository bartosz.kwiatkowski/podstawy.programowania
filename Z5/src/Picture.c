#include "Picture.h"

Flags *flagsGlobal;

void convertPictureUsingFlags(Picture *picture, Flags *flags) {

    flagsGlobal = flags;

    if (flagsGlobal->negative == 1)
        setNegative(picture);

    if (flagsGlobal->threshold == 1)
        setThreshold(picture, flagsGlobal->thresholdScope);

    if (flagsGlobal->halfWhiteThreshold == 1)
        setHalfWhiteThreshold(picture, flagsGlobal->halfWhiteThresholdScope);

    if (flagsGlobal->halfBlackThreshold == 1)
        setHalfBlackThreshold(picture, flagsGlobal->halfBlackThresholdScope);

    if (flagsGlobal->gammaCorection == 1)
        setGammaCorrection(picture, flagsGlobal->gammaCorectionScope);

    if (flagsGlobal->levelCorrection == 1)
        setLevelCorrection(picture, flagsGlobal->levelCorrectionScope[0], 
            flagsGlobal->levelCorrectionScope[1]);

    if (flagsGlobal->conturing == 1)
        setConturing(picture, flagsGlobal->conturingScope);

    if (flagsGlobal->horizontalBlur == 1)
        setHorizontalBlur(picture);

    if (flagsGlobal->verticalBlur == 1)
        setVerticalBlur(picture);

    if (flagsGlobal->histogram == 1)
        setHistogram(picture);
}

void setNegative(Picture *picture) {

    if (isExtensionEqualsInput(P2)) {
        for (int i = 0; i < sizeOfPicture(picture); i++){
            RGB oldColor = getPixelColor(&picture->pixels[i]);
            RGB newColor = initByShade(picture->colorScale - getShadeScale(&oldColor)); 
            setPixelColor(&picture->pixels[i], newColor);
        }
    }

    if (isExtensionEqualsInput(P3)) {
        for (int i = 0; i < sizeOfPicture(picture); i++){
            RGB newColor = getPixelColor(&picture->pixels[i]);

            if (flagsGlobal->red == 1)
                setRedScale(&newColor, picture->colorScale - getRedScale(&newColor));
            if (flagsGlobal->green == 1)
                setGreenScale(&newColor, picture->colorScale - getGreenScale(&newColor));
            if (flagsGlobal->blue == 1)
                setBlueScale(&newColor, picture->colorScale - getBlueScale(&newColor));

            setPixelColor(&picture->pixels[i], newColor);
        }
    }

}

void setThreshold(Picture *picture, int thresholdScope) {
    int scope = (picture->colorScale * thresholdScope) / 100;

    if (isExtensionEqualsInput(P2)) {
        for (int i = 0; i < sizeOfPicture(picture); i++){
            RGB oldColor = getPixelColor(&picture->pixels[i]);

            if (getShadeScale(&oldColor) > scope) {
                setPixelColor(&picture->pixels[i], initByShade(picture->colorScale));
            } else {
                setPixelColor(&picture->pixels[i], initByShade(0));
            }
        }      
    }

    if (isExtensionEqualsInput(P3)) {
        for (int i = 0; i < sizeOfPicture(picture); i++){
            RGB newColor = getPixelColor(&picture->pixels[i]);

            if (getShadeScale(&newColor) > scope) {
                if (flagsGlobal->red == 1)
                    setRedScale(&newColor, picture->colorScale);
                if (flagsGlobal->green == 1)
                    setGreenScale(&newColor, picture->colorScale);
                if (flagsGlobal->blue == 1)
                    setBlueScale(&newColor, picture->colorScale);
            } else {
                if (flagsGlobal->red == 1)
                    setRedScale(&newColor, 0);
                if (flagsGlobal->green == 1)
                    setGreenScale(&newColor, 0);
                if (flagsGlobal->blue == 1)
                    setBlueScale(&newColor, 0);
            }
            setPixelColor(&picture->pixels[i], newColor);
        }
    }
}

void setHalfWhiteThreshold(Picture *picture, int halfThresholdScope) {
    int scope = (picture->colorScale * halfThresholdScope) / 100;

    if (isExtensionEqualsInput(P2)) {
        for (int i = 0; i < sizeOfPicture(picture); i++){
            RGB oldColor = getPixelColor(&picture->pixels[i]);

            if (getShadeScale(&oldColor) > scope) {
                setPixelColor(&picture->pixels[i], initByShade(picture->colorScale));
            } 
        }      
    }

    if (isExtensionEqualsInput(P3)) {
        for (int i = 0; i < sizeOfPicture(picture); i++){
            RGB newColor = getPixelColor(&picture->pixels[i]);

            if (getShadeScale(&newColor) > scope) {
                if (flagsGlobal->red == 1)
                    setRedScale(&newColor, picture->colorScale);
                if (flagsGlobal->green == 1)
                    setGreenScale(&newColor, picture->colorScale);
                if (flagsGlobal->blue == 1)
                    setBlueScale(&newColor, picture->colorScale);
            }
            setPixelColor(&picture->pixels[i], newColor);
        }
    }
}
void setHalfBlackThreshold(Picture *picture, int halfBlackThresholdScope) {

    int scope = (picture->colorScale * halfBlackThresholdScope) / 100;

    if (isExtensionEqualsInput(P2)) {
        for (int i = 0; i < sizeOfPicture(picture); i++){
            RGB oldColor = getPixelColor(&picture->pixels[i]);

            if (getShadeScale(&oldColor) <= scope) {
                setPixelColor(&picture->pixels[i], initByShade(0));
            } 
        }      
    }

    if (isExtensionEqualsInput(P3)) {
        for (int i = 0; i < sizeOfPicture(picture); i++){
            RGB newColor = getPixelColor(&picture->pixels[i]);

            if (getShadeScale(&newColor) <= scope) {
                if (flagsGlobal->red == 1)
                    setRedScale(&newColor, 0);
                if (flagsGlobal->green == 1)
                    setGreenScale(&newColor, 0);
                if (flagsGlobal->blue == 1)
                    setBlueScale(&newColor, 0);
            }
            setPixelColor(&picture->pixels[i], newColor);
        }
    }
}

void setGammaCorrection(Picture *picture, double gamma) {

    if (isExtensionEqualsInput(P2)) {
        for (int i = 0; i < sizeOfPicture(picture); i++){
            RGB oldColor = getPixelColor(&picture->pixels[i]);
            RGB newColor = initByShade(powf(getShadeScale(&oldColor), 1 / gamma) / 
                powf(picture->colorScale, (1 - gamma) / gamma));
            setPixelColor(&picture->pixels[i], newColor);
        }      
    }

    if (isExtensionEqualsInput(P3)) {
        for (int i = 0; i < sizeOfPicture(picture); i++){
            RGB oldColor = getPixelColor(&picture->pixels[i]);
            RGB newColor = initByShade(0);

            if (flagsGlobal->red == 1)
                setRedScale(&newColor, powf(getRedScale(&oldColor), 1 / gamma) / 
                    powf(picture->colorScale, (1 - gamma) / gamma));
            if (flagsGlobal->green == 1)
                setGreenScale(&newColor, powf(getGreenScale(&oldColor), 1 / gamma) / 
                    powf(picture->colorScale, (1 - gamma) / gamma));
            if (flagsGlobal->blue == 1)
                setBlueScale(&newColor, powf(getBlueScale(&oldColor), 1 / gamma) / 
                    powf(picture->colorScale, (1 - gamma) / gamma));
                    
            setPixelColor(&picture->pixels[i], newColor);
        }
    }
}

void setLevelCorrection(Picture *picture, int white, int black) {
    int blackScope = (picture->colorScale * black) / 100;
    int whiteScope = (picture->colorScale * white) / 100;

    if (isExtensionEqualsInput(P2)) {
        for (int i = 0; i < sizeOfPicture(picture); i++){
            RGB oldColor = getPixelColor(&picture->pixels[i]);

            if (getShadeScale(&oldColor) <= blackScope) {
                setPixelColor(&picture->pixels[i], initByShade(0));
            } else if (getShadeScale(&oldColor) >= whiteScope) {
                setPixelColor(&picture->pixels[i], initByShade(picture->colorScale));
            } else {
                int shade = (getShadeScale(&oldColor) - blackScope) * 
                    (picture->colorScale / (whiteScope - blackScope));
                setPixelColor(&picture->pixels[i], initByShade(shade));
            }           
        }      
    }

    if (isExtensionEqualsInput(P3)) {
        for (int i = 0; i < sizeOfPicture(picture); i++){
            RGB oldColor = getPixelColor(&picture->pixels[i]);
            RGB newColor = oldColor;

            if (flagsGlobal->red == 1) {
                if (getRedScale(&oldColor) <= blackScope) {
                    setRedScale(&newColor, 0);
                } else if (getShadeScale(&oldColor) >= whiteScope) {
                    setRedScale(&newColor, picture->colorScale);
                } else {
                    int shade = (getShadeScale(&oldColor) - blackScope) * 
                        (picture->colorScale / (whiteScope - blackScope));
                    setRedScale(&newColor, shade); 
                }
            }

            if (flagsGlobal->green == 1) {
                if (getGreenScale(&oldColor) <= blackScope) {
                    setGreenScale(&newColor, 0);
                } else if (getShadeScale(&oldColor) >= whiteScope) {
                    setGreenScale(&newColor, picture->colorScale);
                } else {
                    int shade = (getShadeScale(&oldColor) - blackScope) * 
                        (picture->colorScale / (whiteScope - blackScope));
                    setGreenScale(&newColor, shade); 
                }    
            }
           
            if (flagsGlobal->blue == 1) {
                if (getBlueScale(&oldColor) <= blackScope) {
                    setBlueScale(&newColor, 0);
                } else if (getShadeScale(&oldColor) >= whiteScope) {
                    setBlueScale(&newColor, picture->colorScale);
                } else {
                    int shade = (getShadeScale(&oldColor) - blackScope) * 
                        (picture->colorScale / (whiteScope - blackScope));
                    setBlueScale(&newColor, shade); 
                } 
            }    
            setPixelColor(&picture->pixels[i], newColor);
        }
    }
}

void setConturing(Picture *picture, int scope) {
    int prog = (picture->colorScale * scope) / 100;
    float border;

    if (isExtensionEqualsInput(P2)) {
        for (int i = 0; i < picture->height; i++) {
            for (int j = 0; j < picture->width; j++) {
                int k = (i * picture->width) + j;
                int m = (i * picture->width) + (j+1);
                int n = ((i+1) * picture->width) + (j);

                if (n < sizeOfPicture(picture) &&  j+1 < picture->width) {
                    
                    RGB actualColor = getPixelColor(&picture->pixels[k]);
                    RGB widthColor = getPixelColor(&picture->pixels[m]);
                    RGB heightColor = getPixelColor(&picture->pixels[n]);

                    border = abs(getShadeScale(&heightColor) - getShadeScale(&actualColor)) +
                        abs(getShadeScale(&widthColor) - getShadeScale(&actualColor));
                    
                    if (border > prog)
                         setPixelColor(&picture->pixels[k], initByShade(picture->colorScale));
                    else
                        setPixelColor(&picture->pixels[k], initByShade(0));
                }
            }
        }      
    }

    if (isExtensionEqualsInput(P3)) {
        for (int i = 0; i < picture->height; i++) {
            for (int j = 0; j < picture->width; j++) {
                int k = (i * picture->width) + j;
                int m = (i * picture->width) + (j+1);
                int n = ((i+1) * picture->width) + (j);

                if (n < sizeOfPicture(picture) &&  j+1 < picture->width) {

                    RGB actualColor = getPixelColor(&picture->pixels[k]);
                    RGB widthColor = getPixelColor(&picture->pixels[m]);
                    RGB heightColor = getPixelColor(&picture->pixels[n]);
                    RGB newColor = actualColor;

                    if (flagsGlobal->red == 1) {
                        border = abs(getRedScale(&heightColor) - getRedScale(&actualColor)) +
                        abs(getRedScale(&widthColor) - getRedScale(&actualColor));
                    
                        if (border > prog)
                            setRedScale(&newColor, picture->colorScale);
                        else
                            setRedScale(&newColor, 0);
                    }

                    if (flagsGlobal->blue == 1) {
                        border = abs(getBlueScale(&heightColor) - getBlueScale(&actualColor)) +
                        abs(getBlueScale(&widthColor) - getBlueScale(&actualColor));
                    
                        if (border > prog)
                            setBlueScale(&newColor, picture->colorScale);
                        else
                            setBlueScale(&newColor, 0);
                    }

                    if (flagsGlobal->green == 1){
                        border = abs(getGreenScale(&heightColor) - getGreenScale(&actualColor)) +
                        abs(getGreenScale(&widthColor) - getGreenScale(&actualColor));
                    
                        if (border > prog)
                            setGreenScale(&newColor, picture->colorScale);
                        else
                            setGreenScale(&newColor, 0);
                    }

                    setPixelColor(&picture->pixels[k], newColor);
                }
            }
        }
    }
}

void setHorizontalBlur(Picture *picture) {
    if (isExtensionEqualsInput(P2)) {
        for (int i = 0; i < picture->height; i++) {
            for (int j = 0; j < picture->width; j++) {
                int k = (i * picture->width) + j;
                int m = (i * picture->width) + (j+1);
                int n = (i * picture->width) + (j-1);

                if (n > 0 &&  (j+1) < picture->width) {
                    RGB oldColor = getPixelColor(&picture->pixels[k]);
                    RGB next = getPixelColor(&picture->pixels[m]);
                    RGB previous = getPixelColor(&picture->pixels[n]);
                    int shade = (getShadeScale(&next) + getShadeScale(&oldColor) +
                        getShadeScale(&previous)) / 3;
                    RGB newColor = initByShade(shade);
                    setPixelColor(&picture->pixels[k], newColor);
                }
            }
        }      
    }

    if (isExtensionEqualsInput(P3)) {
        for (int i = 0; i < picture->height; i++) {
            for (int j = 0; j < picture->width; j++) {
                int k = (i * picture->width) + j;
                int m = (i * picture->width) + (j+1);
                int n = (i * picture->width) + (j-1);

                if (n > 0 &&  (j+1) < picture->width) {
                    RGB oldColor = getPixelColor(&picture->pixels[k]);
                    RGB next = getPixelColor(&picture->pixels[m]);
                    RGB previous = getPixelColor(&picture->pixels[n]);
                    int red = (getRedScale(&next) + getRedScale(&oldColor) +
                        getRedScale(&previous)) / 3;
                    int green = (getGreenScale(&next) + getGreenScale(&oldColor) +
                        getGreenScale(&previous)) / 3;
                    int blue = (getBlueScale(&next) + getBlueScale(&oldColor) +
                        getBlueScale(&previous)) / 3;

                    RGB newColor = initByRGB(red, green, blue);
                    setPixelColor(&picture->pixels[k], newColor);
                }
            }
        }      
    }
}
void setVerticalBlur(Picture *picture) {
    if (isExtensionEqualsInput(P2)) {
        for (int i = 0; i < picture->height; i++) {
            for (int j = 0; j < picture->width; j++) {
                int k = (i * picture->width) + j;
                int m = ((i-1) * picture->width) + j;
                int n = ((i+1) * picture->width) + j;

                if (n < sizeOfPicture(picture) && i > 0) {
                    RGB oldColor = getPixelColor(&picture->pixels[k]);
                    RGB next = getPixelColor(&picture->pixels[m]);
                    RGB previous = getPixelColor(&picture->pixels[n]);
                    int shade = (getShadeScale(&next) + getShadeScale(&oldColor) +
                        getShadeScale(&previous)) / 3;
                    RGB newColor = initByShade(shade);
                    setPixelColor(&picture->pixels[k], newColor);
                }
            }
        }      
    }

    if (isExtensionEqualsInput(P3)) {
        for (int i = 0; i < picture->height; i++) {
            for (int j = 0; j < picture->width; j++) {
                int k = (i * picture->width) + j;
                int m = ((i-1) * picture->width) + j;
                int n = ((i+1) * picture->width) + j;

                if (n < sizeOfPicture(picture) && i > 0) {
                    RGB oldColor = getPixelColor(&picture->pixels[k]);
                    RGB next = getPixelColor(&picture->pixels[m]);
                    RGB previous = getPixelColor(&picture->pixels[n]);
                    int red = (getRedScale(&next) + getRedScale(&oldColor) +
                        getRedScale(&previous)) / 3;
                    int green = (getGreenScale(&next) + getGreenScale(&oldColor) +
                        getGreenScale(&previous)) / 3;
                    int blue = (getBlueScale(&next) + getBlueScale(&oldColor) +
                        getBlueScale(&previous)) / 3;

                    RGB newColor = initByRGB(red, green, blue);
                    setPixelColor(&picture->pixels[k], newColor);
                }
            }
        }      
    }
}

void setHistogram(Picture *picture) {
    if (isExtensionEqualsInput(P2)) {
        int min = picture->colorScale;
        int max = 0;
        
        for (int i = 0; i < sizeOfPicture(picture); i++){
            RGB oldColor = getPixelColor(&picture->pixels[i]);
            if (min > getShadeScale(&oldColor))
                min = getShadeScale(&oldColor);
            if (max < getShadeScale(&oldColor))
                max = getShadeScale(&oldColor);
        }

        for (int i = 0; i < sizeOfPicture(picture); i++){
            RGB oldColor = getPixelColor(&picture->pixels[i]);
            int shade = (getShadeScale(&oldColor) - min) * 
                (picture->colorScale / (max - min));
            RGB newColor = initByShade(shade);
        }
    }

    if (isExtensionEqualsInput(P3)) {
        int min[3] = {picture->colorScale, picture->colorScale, picture->colorScale};
        int max[3] = {0, 0, 0};
        
        for (int i = 0; i < sizeOfPicture(picture); i++){
            RGB oldColor = getPixelColor(&picture->pixels[i]);

            if (min[0] > getRedScale(&oldColor))
                min[0] = getRedScale(&oldColor);
            if (max[0] < getRedScale(&oldColor))
                max[0] = getRedScale(&oldColor);
            
            if (min[1] > getGreenScale(&oldColor))
                min[1] = getGreenScale(&oldColor);
            if (max[1] < getGreenScale(&oldColor))
                max[1] = getGreenScale(&oldColor);

            if (min[2] > getBlueScale(&oldColor))
                min[2] = getBlueScale(&oldColor);
            if (max[2] < getBlueScale(&oldColor))
                max[2] = getBlueScale(&oldColor);
        }

        for (int i = 0; i < sizeOfPicture(picture); i++){
            RGB oldColor = getPixelColor(&picture->pixels[i]);
            int red = (getRedScale(&oldColor) - min[0]) * 
                (picture->colorScale / (max[0] - min[0]));
            int green = (getGreenScale(&oldColor) - min[1]) * 
                (picture->colorScale / (max[1] - min[1]));
            int blue = (getBlueScale(&oldColor) - min[2]) * 
                (picture->colorScale / (max[2] - min[2]));
            
            RGB newColor = initByRGB(red, green, blue);
        }
    }
}

int sizeOfPicture(Picture *picture) {
    return picture->width * picture->height;
}