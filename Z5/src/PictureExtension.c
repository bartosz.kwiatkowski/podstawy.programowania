#include "PictureExtension.h"

// SIGNATURES HEXADECIMAL
const char PGM_HEX[] = "5032A";
const char PPM_HEX[] = "5033A";
const char JPG_HEX[] = "FFD8ff";


// METHODS
void defineFileFormat(FILE *file, char *output) {
    BYTE fileSignatures[3];
    char signatureInHex[10];

    fread(fileSignatures, sizeof(BYTE), 3, file);
    sprintf(signatureInHex, "%X%X%X", fileSignatures[0], fileSignatures[1],
        fileSignatures[2]);
    
    strcpy(output, compareFileExtension(signatureInHex));
}

char* compareFileExtension(char *extension) {

    if (strcmp(extension, PGM_HEX) == 0)
        return "P2";

    if (strcmp(extension, PPM_HEX) == 0)
        return "P3";

    if (strcmp(extension, JPG_HEX) == 0)
        return "JPG";
}