#include "Picture.h"

void throwError(char message[], int number) {
    printf("[ERROR %d] %s \n", number, message);
    exit(number);
}