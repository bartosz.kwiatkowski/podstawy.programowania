#include "Flags.h"

Flags initializeArgumentParser(int argc, char ** argv) {
    Flags flags;
    flags.fileNameInput = NULL;
    flags.fileNameOutput = NULL;
    flags.thresholdScope = 1;
    flags.halfWhiteThresholdScope = 1;
    flags.halfBlackThresholdScope = 1;
    flags.gammaCorectionScope = 1;
    flags.levelCorrectionScope[0] = 1;
    flags.levelCorrectionScope[1] = 1;
    flags.conturingScope = 1;

    defineArguments(&flags, argc, argv);
    validateParsedArguments(&flags);

    return flags;
}

void defineArguments(Flags *input, int argc, char ** argv) {
    char option;

    while (( option = getopt(argc, argv, "i:no:c:t:w:b:g:l:k:xyh")) != -1) {
        switch (option) {
        case 'i':
            input->fileNameInput = optarg;
            break;
        case 'n':
            input->negative = 1;
            break;
        case 'o':
            input->fileNameOutput = optarg;
            break;
        case 'c':
            optarg[0] = toupper((unsigned char) optarg[0]);
            input->red = isRed(optarg[0]);
            input->green = isGreen(optarg[0]);
            input->blue = isBlue(optarg[0]);
            break;
        case 't':
            input->thresholdScope = atoi(optarg);
            input->threshold = 1;
            break;
        case 'w':
            input->halfWhiteThresholdScope = atoi(optarg);
            input->halfWhiteThreshold = 1;
            break;
        case 'b':
            input->halfBlackThresholdScope = atoi(optarg);
            input->halfBlackThreshold = 1;
            break;
        case 'g':
            input->gammaCorectionScope = atof(optarg);
            input->gammaCorection = 1;
            break;
        case 'l':
            input->levelCorrectionScope[0] = atoi(optarg);
            input->levelCorrectionScope[1] = atoi(argv[optind]);
            input->levelCorrection = 1;
            break;
        case 'k':
            input->conturingScope = atoi(optarg);
            input->conturing = 1;
            break;
        case 'x':
            input->verticalBlur = 1;
            break;
        case 'y':
            input->horizontalBlur = 1;
            break;
        case 'h':
            input->horizontalBlur = 1;
            break;
        case '?':
        default:
            printf ("Help: \n", argv[0]);
        }
    }
}

void validateParsedArguments(Flags *input) {
    
    if (input->fileNameInput == NULL || input->fileNameOutput == NULL)
        throwError("Input/Output is not complete!", -1);

    if (input->thresholdScope < 0 ||
        input->thresholdScope > 100)
        throwError("Threshold scope is between 0-100!", -1);

    if (input->halfWhiteThresholdScope < 0 || 
        input->halfWhiteThresholdScope > 100)
        throwError("Half white threshold scope is between 0-100!", -1);

    if (input->halfBlackThresholdScope < 0 || input->halfBlackThresholdScope > 100)
        throwError("Half black threshold scope is between 0-100!", -1);

    if (input->gammaCorectionScope < 0)
        throwError("Gamma corection scope is a positive value!", -1);

    if (input->levelCorrectionScope[0] < 0 || input->levelCorrectionScope[0] > 100 ||
        input->levelCorrectionScope[1] < 0 || input->levelCorrectionScope[1] > 100)
        throwError("Leveling Correction scope is between 0-100!", -1);

    if (input->conturingScope < 0 || input->conturingScope > 100)
        throwError("Conturing scope is between 0-100!", -1);

}

int isRed(char c) {
    if (c == 'R' || c == 'S')
        return 1;
    else 
        return 0;
}

int isGreen(char c){
    if (c == 'G' || c == 'S')
        return 1;
    else 
        return 0;
}

int isBlue(char c){   
    if (c == 'B' || c == 'S')
        return 1;
    else 
        return 0;
}