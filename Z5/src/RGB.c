#include "RGB.h" 

// PUBLIC
RGB initByRGB(unsigned char red, unsigned char green, unsigned char blue) {
    RGB rgb;

    setRedScale(&rgb, red);
    setGreenScale(&rgb, green);
    setBlueScale(&rgb, blue);

    return rgb;
}

RGB initByShade(short shade) {
    RGB rgb;

    setShadeScale(&rgb, shade);

    return rgb;
}

// PRIVATE
int getNumberByIndex(RGB *RGB, int id) {
    return (int) RGB->colorScale[id];
}

// GETTERS
short getRedScale(RGB *RGB) {
    return getNumberByIndex(RGB, 0);
}
short getGreenScale(RGB *RGB) {
    return getNumberByIndex(RGB, 1);
}

short getBlueScale(RGB *RGB) {
    return getNumberByIndex(RGB, 2);
}

short getShadeScale(RGB *RGB) {
    return getNumberByIndex(RGB, 0) + 
        getNumberByIndex(RGB, 1) + getNumberByIndex(RGB, 2);
}

// Setters
void setRedScale(RGB *RGB, unsigned char number) {
    RGB->colorScale[0] = number;
}

void setGreenScale(RGB *RGB, unsigned char number) {
    RGB->colorScale[1] = number;
}

void setBlueScale(RGB *RGB, unsigned char number) {
    RGB->colorScale[2] = number;
}

void setShadeScale(RGB *RGB, int number) {
    if (number > 765 || number < 0) return;

    RGB->colorScale[0] = 0;
    RGB->colorScale[1] = 0;
    RGB->colorScale[2] = 0;
    
    if (number <= 255) RGB->colorScale[0] = (unsigned char) number;
    else if (number > 255 && number <= 510){
        RGB->colorScale[0] = 255;
        number -= 255;
        RGB->colorScale[1] = number;
    } else {
        RGB->colorScale[0] = 255;
        number -= 255;
        RGB->colorScale[1] = 255;
        number -= 255;
        RGB->colorScale[2] = number;
    }
}
