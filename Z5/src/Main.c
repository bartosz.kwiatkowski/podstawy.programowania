#include "Picture.h"

int main (int argc, char ** argv) {
    
    Flags flags = initializeArgumentParser(argc, argv);
    
    Picture pic = initializePicture(flags.fileNameInput);
    
    convertPictureUsingFlags(&pic, &flags);
    savePictureToFile(pic, flags.fileNameOutput);

    free(pic.pixels);
    
    return 0;
}