#include "PictureIO.h"

Picture pictureIO;

Picture initializePicture(char fileName[]) {
    FILE *file;
    
    file = fopen(fileName, "rw");
    if (file == NULL) throwError("File not found !", -1);

    defineFileFormat(file, pictureIO.fileExtension);
    readPictureFromFile(file);

    fclose(file);
    
    return pictureIO;
}

void readPictureFromFile(FILE *input) {
    const int BUFFOR_SIZE = 1024;
    char buffor[BUFFOR_SIZE];

    if (input == NULL) return;
    if (fgets(buffor, BUFFOR_SIZE, input) == NULL);
    
    readPictureSettings(input);

    pictureIO.pixels = malloc(pictureIO.width * pictureIO.height * sizeof(Pixel));

    readPixelsFromFile(input);
}

void savePictureToFile(Picture input, char fileName[]) {
    pictureIO = input;
    FILE *output;
    output = fopen(fileName, "w+");

    savePictureSettings(output);
    savePixelsToFile(output);

    fclose(output);
}

void readPixelsFromFile(FILE *input) {
    int sizeOfPicture = pictureIO.width * pictureIO.height;

    if (isExtensionEqualsInput(P2)) {
        for (int i = 0; i < sizeOfPicture; i++){
            int shade;
            fscanf(input, "%3d", &shade);
            setPixelColor(&pictureIO.pixels[i], initByShade(shade));
        }
    }

    if (isExtensionEqualsInput(P3)){
        for (int i = 0; i < sizeOfPicture; i++) {
            int r,g,b;
            fscanf(input, "%d %d %d", &r, &g, &b);
            setPixelColor(&pictureIO.pixels[i], initByRGB(r, g, b));
        }
    }

}

void savePixelsToFile(FILE *output) {
    int sizeOfPicture = pictureIO.width * pictureIO.height;

    if (isExtensionEqualsInput(P2)) {
        for (int i = 0; i < sizeOfPicture; i++){
            RGB tmp = getPixelColor(&pictureIO.pixels[i]);
            fprintf(output, "%3d ", getShadeScale(&tmp));

            if ((i +1) % 18 == 0) fprintf(output, "\n");
        }
    }

    if (isExtensionEqualsInput(P3)){
        for (int i = 0; i < sizeOfPicture; i++){
            RGB tmp = getPixelColor(&pictureIO.pixels[i]);
            fprintf(output, "%3d %3d %3d ", 
                getRedScale(&tmp), getGreenScale(&tmp), getBlueScale(&tmp));

            if ((i +1) % 18 == 0) fprintf(output, "\n");
        }
    }
}

void readPictureSettings(FILE *input) {

    if (isExtensionEqualsInput(P2) || isExtensionEqualsInput(P3)) {
        fscanf(input, "%d %d %d",
            &pictureIO.width, &pictureIO.height, &pictureIO.colorScale);
    }
}

void savePictureSettings(FILE *output){
    if (isExtensionEqualsInput(P2) || isExtensionEqualsInput(P3)) {
        fprintf(output, "%s\n", pictureIO.fileExtension);
        fprintf(output, "%d %d\n", pictureIO.width, pictureIO.height);
        fprintf(output, "%d\n", pictureIO.colorScale);
    }
}

bool isExtensionEqualsInput(const char *input) {
    if (strcmp(pictureIO.fileExtension, input) == 0) return true;
    else return false;
}



