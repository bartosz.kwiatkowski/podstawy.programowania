#include <stdio.h>


int nwd(int a, int b){

    int rest = a % b;
    if (rest == 0) return b;
    else return nwd(b, rest);
}

int main(){

    int x = 20;
    int y = 30; 
    printf("NWD(%d;%d) = %d \n", x, y, nwd(x, y));
    
    return 0;
}
