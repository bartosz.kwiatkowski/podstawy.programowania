#include <stdio.h>
#include <stdlib.h>


float power(int number, int pow){

    char powMinus = '0';
    if (pow < 0){
        powMinus = '1';
        pow *= -1;
    } 

    int result = 1;

    for (int i = 0; i < pow; i++){
        result *= number;
    }
    if (powMinus == '1') return (float) 1/result;   
    return (float) result;
}

float powerS(int number, int pow){ 

    char powMinus = '0';
    if (pow < 0){
        powMinus = '1';
        pow *= -1;
    } 

    if (pow == 0) return 1;

    int result = number;
    int clone = number;

    for (int i = 1; i < pow; i++){
        for (int j = 1; j < number; j++) {
            result += clone;
        }
        clone = result;
    }

    if (powMinus == '1') return (float) 1 / result;

    return result;
    
}

int main(){

    printf("potega: %lf \n", powerS(2,-2));
    printf("potega: %lf \n", powerS(2,0));
    printf("potega: %lf \n", powerS(2,3));
    puts("");
    printf("potega: %lf \n", power(2,-2));
    printf("potega: %lf \n", power(2,0));
    printf("potega: %lf \n", power(2,3));

    return 0;
}