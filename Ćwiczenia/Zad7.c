#include <stdio.h>

int main(){

    int k = 0;
    char string[100];
    char ch;
    while ((ch = getchar()) != '\n' && ch != EOF){
        if (ch != ' '){
            string[k] = ch;
            k++;
        }
    }

    for (int i = 0; i < k; i++){
        if (string[i] != string[k-1]){
            printf("%s Nie jest palindromem! \n", string);
            return 1;
        }
        k--;
    }

    printf("%s Jest palindromem! \n", string);
    return 0 ;
}