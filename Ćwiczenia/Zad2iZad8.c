#include<stdio.h>


int fib(int n);
void setPascalTriangle(int *array, int n);

int main(){
    
    int n;

    printf("Podaj numer ciagu fibonacciego: ");
    scanf("%d",&n);
    printf("%d numer ciagu fibonnacciego to %d \n", n , fib(n));

    return 0 ;
}

void setPascalTriangle(int *array, int n){

    for(int i = 0; i < n; i++){
        printf("%2d. ", i + 1);
        for(int j = 0; j <= i ; j++){
            if(j == 0 || j == i) *((array + i*n) + j) = 1;
            else *((array + i*n) + j) = *((array  + (i-1) * n) + (j-1)) +
                *((array + (i-1) * n) + j);
            printf("%3d ", *((array+i*n) + j));
        }
       printf("\n");
    }
}

int fib(int n){

    int pascalTriangle[n][n];

    setPascalTriangle((int *) pascalTriangle, n);
    
    puts("");
    puts("");

    int sum = 0;

    int j = 0;
    for (int i = n; i > 0; i--){
        sum += pascalTriangle[i-1][j];
        
        if (i != n && pascalTriangle[i-1][j] == 1) break;
        j++;
    }

    return sum;
}