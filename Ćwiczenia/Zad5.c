#include <stdio.h>
#define SIZE 5

int block [3][SIZE];

void setUp(int x, int y);
void printGame();

// wyznaczanie zlozonosci wiezy

int main(void){

    for (int i = 0; i < SIZE; i++){
        block[0][i] = i+1;
    }

    printGame();

    while (1){
        int x, y;
        printf("Przenies z x do y: ");
        scanf("%d %d", &x, &y);

        setUp(x, y);

        printGame();
    }

}

void setUp(int x, int y){
    
    x--;
    y--;
    int variable;
    for (int i = 0; i < SIZE; i++){
        if (block[x][i] != 0){
            for (int j = SIZE-1; j >= 0; j--){
                if (block[y][j] == 0){
                    if (block[y][j+1] > block[x][i] || block[y][SIZE-1] == 0){
                        block[y][j] = block[x][i];
                        block[x][i] = 0;
                        return;
                    }
                } 
            }
        }
    }
}

void printGame(){
    printf("A ");
    for (int i = 0; i < SIZE; i++){
        printf("%d", block[0][i]);
    }
    puts("");
    printf("B ");
    for (int i = 0; i < SIZE; i++){
        printf("%d", block[1][i]);
    }
    puts("");
    printf("C ");
    for (int i = 0; i < SIZE; i++){
        printf("%d", block[2][i]);
    }
    puts("");
}