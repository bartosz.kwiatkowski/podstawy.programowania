#include "RPN.h"

Stack *operation(Stack *stack, char n) {
    int x, y;

    if (empty(stack) != 1) 
        stack = pop(stack, &x);
    if (empty(stack) != 1) 
        stack = pop(stack, &y);
    else {
        stack = push(stack, x);
        return stack;
    }

    switch (n) {
    case '+':
        stack = push(stack, y + x);
        break;
    case '-':
        stack = push(stack, y - x);
        break;
    case '*':
        stack = push(stack, y * x);
        break;
    case '/':
        if (x != 0)
            stack = push(stack, y / x);
        else {
            stack = push(stack, y);
            stack = push(stack, x);
        }
        break;
    case '%':
         stack = push(stack, y % x);
        break;
    }

    return stack; 
}