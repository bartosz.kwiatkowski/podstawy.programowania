#include "RPN.h"
#include "cs50.h"

// kompilacja      | R, M
// niekomilacja    | A 
// wszystko dobrze | B


int main() {
    
    Stack *stack = NULL;
    string line;
    int number;

    do {
        line = get_string("");
        
        if ((number = atoi(line)) == 0 && line[0] != '0') {

            switch (line[0]) {
            case '+':
                stack = operation(stack, '+');
                break;
            case '-':
                stack = operation(stack, '-');
                break;
            case '*':
                stack = operation(stack, '*');
                break;
            case '/':
                stack = operation(stack, '/');
                break;
            case '^':
                stack = operation(stack, '^');
                break;
            case '%':
                stack = operation(stack, '%');
                break;
            case 'P':
                stack = pop(stack, &number);
                break;
            case 'c':
                stack = delete(stack);
                break;
            case 'r':
                reverse(stack);
                break;
            case 'd':
                stack = duplicate(stack);
                break;
            case 'p':
                showTop(stack);
                break;
            case 'f':
                display(stack);
                break;
            }
        } else {
            stack = push(stack, number);
        }

    } while (line[0] != 'q');
    
    
}