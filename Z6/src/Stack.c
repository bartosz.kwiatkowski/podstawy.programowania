#include "Stack.h"

 
Stack* push(Stack* head, int data) {
    Stack* tmp = (Stack*)malloc(sizeof(Stack));
    if(tmp == NULL) {
        exit(0);
    }
    tmp->data = data;
    tmp->next = head;
    head = tmp;
    return head;
}

Stack* pop(Stack *head,int *element) {
    if (head == NULL) return NULL;
    Stack* tmp = head;
    *element = head->data;
    head = head->next;
    free(tmp);
    return head;
}

int empty(Stack* head) {
    return head == NULL ? 1 : 0;
}
 
void display(Stack* head) {
    Stack *current;
    current = head;
    if(current!= NULL)
    {
        printf("Stack: ");
        do
        {
            printf("%d ",current->data);
            current = current->next;
        }
        while (current!= NULL);
        printf("\n");
    }
    else
    {
        printf("The Stack is empty\n");
    }
 
}

Stack* delete(Stack *stack) {
    int tmp;
    while (stack->next != NULL) {
        stack = pop(stack, &tmp);
    }
    stack = pop(stack, &tmp);

    return stack;
}

void reverse(Stack *stack) {
    int x, y;

    if (empty(stack) != 1)
        stack = pop(stack, &x);
    if (empty(stack) != 1)
        stack = pop(stack, &y);
    else {
        stack = push(stack, x);
        return;
    }

    stack = push(stack, x);
    stack = push(stack, y);
}

Stack* duplicate(Stack *stack) {
    int x;

    if (empty(stack) != 1)
        stack = pop(stack, &x);
    else 
        return stack;
    
    stack = push(stack, x);
    stack = push(stack, x);

    return stack;
}

void showTop(Stack *stack) {
    int x;

    if (empty(stack) != 1)
        stack = pop(stack, &x);
    else 
        return;
    
    stack = push(stack, x);
    printf("%d \n", x);
}