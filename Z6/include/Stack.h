#include <stdio.h>
#include <stdlib.h>

struct Stack {
    int data;
    struct Stack* next;
};

typedef struct Stack Stack;

int empty(Stack *s);
struct Stack* push(Stack *s,int data);
struct Stack* pop(Stack *s,int *data);
void display(Stack* head);
Stack* delete(Stack *stack);
void reverse(Stack *stack);
Stack* duplicate(Stack *stack) ;
void showTop(Stack *stack);