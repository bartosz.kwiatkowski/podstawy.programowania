#include <stdio.h>
#include <math.h>
#include <complex.h>
#define GET(x) printf(#x " = "); scanf("%le", &x)

void printComplex(double complex z);
void squareTrinomal(double a, double b, double c);

int main(int argc, char *argv[]) {
  
  double a,b,c;

  if (argc == 1){
    puts("Podaj wspolczynniki wedlug wzoru:");
    puts("  ax^+bx+c"); puts("");
    GET(a);
    GET(b); 
    GET(c);
  } else if (argc == 4){
    sscanf(argv[1], "%le", &a);
    sscanf(argv[2], "%le", &b);
    sscanf(argv[3], "%le", &c);
  }

  squareTrinomal(a,b,c);

  return 0;

}

void squareTrinomal(double a, double b, double c){
  
  if (a == 0){ //sprawdzanie wartosci a = 0 ?
    if (b == 0){ //sprawdzanie wartosci b = 0 ?
      if (c == 0) //sprawdzanie wartosci c = 0 ?
        puts("Rozwiazanie dla wszystkich liczb rzeczywistych.");
      else 
        puts("Brak rozwiazan.");
    } else
      printf("Rozwiazaniem jest x = %f\n", c/-b);
  } else {
    double delta = pow(b, 2) - 4*a*c; //liczymy delte
    if (delta == 0){ //sprawdzanie delta = 0 ?
      printf("Rozwiazaniem rownania jest x = %f\n", -b/(2*a));
    } else if (delta > 0){
      puts("Rozwiazaniem rownania jest: \n");
      printf("x1 = %f\n", (-b - sqrt(delta)) / (2*a));
      printf("x2 = %f\n", (-b + sqrt(delta)) / (2*a));
    } else if (delta < 0){
      double complex deltaComplex = csqrt(delta); //zapisanie w postaci l.zespolonej
      double complex z1 = (-b - deltaComplex) / (2*a);
      double complex z2 = (-b + deltaComplex) / (2*a);
      puts("Rozwiazaniem rownania jest: \n");
      printf("x1 = ");
      printComplex(z1);
      printf("x1 = ");
      printComplex(z2);
    }
  }
}

void printComplex(double complex z){
  
  if (cimag(z) == 0){
    printf("%f", creal(z));
  } else if (cimag(z) == 1){
    printf("%f + i", creal(z));
  } else if (cimag(z) == -1){
    printf("%f - i", creal(z));
  } else if (cimag(z) > 0){
    printf("%f + %fi", creal(z), cimag(z));
  } else if (cimag(z) < 0){
    printf("%f - %fi", creal(z), cimag(z)*-1);
  }
  printf("\n");
}