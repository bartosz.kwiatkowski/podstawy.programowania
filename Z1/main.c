#include <stdio.h>



//funkcja matemtycznej prostej; zwraca dwie wartosci, wspolczynnik kierunkowy i f(0) = b
double * straightLine(double x1, double y1, double x2, double y2)
{ 
    //returnValue przechowuje wspolczynnik a, oraz f(0) = b
    static double returnValue[2];
    returnValue[0] = (y2 - y1)/(x2 - x1); //wartosc a
    returnValue[1] = y1 - (returnValue[0]*x1); // wartosc b

    return returnValue;
}

int main(int argc, char *argv[]) {

  //przyjmuje wartosci double dla punktow A..D, x oraz y do tablicy 2 elementowej
  double A[2];
  double B[2];
  double C[2];
  double D[2];

  printf("Podaj wspolrzedna x i y punktu A: ");
  scanf("%lf%lf", &A[0], &A[1]);

  printf("Podaj wspolrzedna x i y punktu B: ");
  scanf("%lf%lf", &B[0], &B[1]);

  printf("Podaj wspolrzedna x i y punktu C: ");
  scanf("%lf%lf", &C[0], &C[1]);

  printf("Podaj wspolrzedna x i y punktu D: ");
  scanf("%lf%lf", &D[0], &D[1]);

  //Wyznaczam dwie proste, na ktorych znajduja sie odcinki

  //PROSTA 1
  double * straightLineValue;
  straightLineValue = straightLine(A[0], A[1], B[0], B[1]); 
  
  double Y1[2];
  Y1[0] = straightLineValue[0];
  Y1[1] = straightLineValue[1];

  //PROSTA 2
  straightLineValue = straightLine(C[0], C[1], D[0], D[1]); 

  double Y2[2];
  Y2[0] = straightLineValue[0];
  Y2[1] = straightLineValue[1];


  //punkt przeciecia prostych
  double W[2];

  //sprawdzam czy nie dziele przez zero | obliczam punkt przeciecia W, wartosc x
  if (Y1[0]-Y2[0] == 0){
    W[0] = 0;
  } else {
    W[0] = (Y2[1]-Y1[1]) / (Y1[0]-Y2[0]);
  }

  //obliczam punkt przeciecia W, wartosc y
  W[1] = Y1[0]*W[0] + Y1[1];

  // Jezeli wspolczynniki sa jednakowe, odcinki leza na jednej prostej
  if (Y1[0] == Y2[0]) {
    
    //sprawdzamy 4 sytuacje gdy odcinki leza na jednej prostej i moga sie przecinac
    if ( (C[0] < A[0] || C[0] < B[0]) && (A[0] == D[0] && A[1] == D[1]) ){
      printf("Odcinek AB i BC sie przecinaja! "); 
      printf("W punkcie A,D = [ %lf ; %lf] \n", A[0], A[1]);
      return 0;
    } else if ( (D[0] < A[0] || D[0] < B[0]) && (A[0] == C[0] && A[1] == C[1]) ) {
      printf("Odcinek AB i BC sie przecinaja! "); 
      printf("W punkcie A,D = [ %lf ; %lf] \n", A[0], A[1]);
      return 0;
    } else if ( (D[0] > A[0] || D[0] > B[0]) && (B[0] == C[0] && B[1] == C[1]) ) {
      printf("Odcinek AB i BC sie przecinaja! "); 
      printf("W punkcie C,B = [ %lf ; %lf] \n", C[0], C[1]);
      return 0;
    }else if  ( (C[0] > A[0] || C[0] > B[0]) && (B[0] == D[0] && B[1] == D[1]) ) {
      printf("Odcinek AB i BC sie przecinaja! "); 
      printf("W punkcie B,D = [ %lf ; %lf] \n", B[0], B[1]);
      return 0;
    } else {
      printf("Odcinek AB i BC sie nie przecinaja!\n");
      return 0;
    }
  }
  
  // Jezeli wspolczynniki sa rozne, proste na ktorych leza odc maja punkt przeciecia (mozliwe miejsce przeciecia odc)
  if ( (W[0] >= A[0] && W[0] <= B[0]) && (W[0] >= C[0] && W[0] <= D[0]) ){
    printf("Odcinek AB i BC sie przecinaja. ");
    printf("W punkcie W = [ %lf ; %lf] \n", W[0], W[1]);
    return 0;
  } else if ( (W[0] >= B[0] && W[0] <= A[0]) && (W[0] >= C[0] && W[0] <= D[0]) ){
    printf("Odcinek AB i BC sie przecinaja. ");
    printf("W punkcie W = [ %lf ; %lf] \n", W[0], W[1]);
    return 0;
  } else if ( (W[0] >= B[0] && W[0] <= A[0]) && (W[0] >= D[0] && W[0] <= D[0]) ){
    printf("Odcinek AB i BC sie przecinaja. ");
    printf("W punkcie W = [ %lf ; %lf] \n", W[0], W[1]);
    return 0;
  } else {
      printf("Odcinek AB i BC sie nie przecinaja!\n");
      return 0;
  }

  return 0;
}


