#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define RESOLUTION 512
#define BUFFOR_SIZE 1024
#define COMMENT_SIZE 100


struct File {
    int picture[RESOLUTION][RESOLUTION];
    int width;
    int height;
    int shades;
    char comment[COMMENT_SIZE];
};

typedef struct File File;

void constructor(char name[]);
void readFile(FILE *fileOpen);
void saveFile(char fileName[]);
File convertToNegative();
File thresholding(int scope);
File halfBlackThresholding(int scope);
File halfWhiteThresholding(int scope);
File gammaCorection(float gamma);
File levelCorection(int black, int white);
File conturing(int scope);
File horizontalBlur();
File verticalBlur();
File stretchingHistogram();
File splotFilter(int nr);
void addComment(char string[]);
void throwError(char message[], int number);
