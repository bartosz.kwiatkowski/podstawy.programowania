#include <stdio.h>
#include <stdlib.h>

int array[99][99];

void fillArray(int m, int n);
void printArray(int m, int n);
void swapSignArray(int m, int n);

int main(){

    fillArray(3,7);

    puts("Zawartosc oryginalna:");

    printArray(3,7);
    swapSignArray(3,7);

    puts("Zawartosc po zmianie znakow:");
    printArray(3,7);

    return 0;
}

void fillArray(int m, int n){

    for (int i = 0; i < m; i++){
        for (int j = 0; j < n; j++){
            array[i][j] = i*j;
        }
    }
}

void printArray(int m, int n){

    for (int i = 0; i < m; i++){
        for (int j = 0; j < n; j++){
            printf("%3d  ", array[i][j]);
            if (j == n-1) printf("\n");
        }
    }
}

void swapSignArray(int m, int n){
    for (int i = 0; i < m; i++){
        for (int j = 0; j < n; j++){
            array[i][j] = (-1)*array[i][j];
        }
    }
}

