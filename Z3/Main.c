#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include "File.h"

#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

#if defined(_WIN32) || defined(_WIN64)
    #define clear(); system("cls");
#else 
    #define clear(); system("clear");
#endif

char defaultFileName[100];

void debugger(char fileName[]);
void menu();

int main(int argc, char* argv[]){

    clear();
    
    #ifdef DEBUG
        debugger(argv[1]);
    #else
        printf("Enter file name: ");
        scanf("%[^\n]", &defaultFileName);
        constructor(defaultFileName);
        menu();
    #endif

    return 0;
}

void menu(){

    char isFileSave = '0';
    char name[100];
    int scope;
    float gamma;
    int black, white;

    while (1) {
        char option = 0;

        clear();
        
        puts(">>>   MENU   <<<");
        puts(" 1 - Negative");
        puts(" 2 - Thresholding");
        puts(" 3 - Half Black Thresholding");
        puts(" 4 - Half White Thresholding");
        puts(" 5 - Gamma Corection");
        puts(" 6 - Level Corection");
        puts(" 7 - Conturing");
        puts(" 8 - Horizontal Blur");
        puts(" 9 - Vertical Blur");
        puts("A - Stretching Histogram");
        puts("B - Splot Filter");
        puts(">>>      <<<");
        puts(" s - Save");
        puts(" z - Save as");
        puts(" x - Exit");
        puts(">>>      <<<");

        scanf(" %c", &option);
        clear();

        switch (option){
            case '1':
                isFileSave = '0';
                puts("Covert to negative. \n");
                convertToNegative();
                break;
            case '2':
                isFileSave = '0';
                printf("Thresholding.\n");
                do{
                    printf("Enter scope [0-100]: ");
                    scanf("%d", &scope);
                } while (scope > 100 || scope < 0);
                thresholding(scope);
                break;
            case '3':
                isFileSave = '0';
                printf("Half Black Thresholding. \n");
                do{
                    printf("Enter scope [0-100]: ");
                    scanf("%d", &scope);
                } while (scope > 100 || scope < 0);
                halfBlackThresholding(scope);
                break;
            case '4':
                isFileSave = '0';
                printf("Half White Thresholding. \n");
                do{
                    printf("Enter scope [0-100]: ");
                    scanf("%d", &scope);
                } while (scope > 100 || scope < 0);
                halfWhiteThresholding(scope);
                break;
            case '5':
                isFileSave = '0';
                printf("Gamma correction. \n");
                do{
                    printf("Enter scope [0.00-100.00]: ");
                    scanf("%f", &gamma);
                } while (gamma > 100 || gamma < 0);
                gammaCorection(gamma);
                break;
            case '6':
                isFileSave = '0';
                printf("Level corection. \n");
                do{
                    printf("Enter black and white [0-100]: ");
                    scanf("%d %d", &black, &white);
                } while (black > 100 || black < 0 || white > 100 || white < 0);
                levelCorection(black, white);
                break;
            case '7':
                isFileSave = '0';
                printf("Conturing.");
                do{
                    printf("Enter scope [0-100]: ");
                    scanf("%d %d", &scope);
                } while (scope > 100 || scope < 0);
                conturing(scope);
                break;

            case '8':
                isFileSave = '0';
                printf("Horizontal Blur.");
                horizontalBlur();
                break;
            case '9':
                isFileSave = '0';
                printf("Vertical Blur.");
                verticalBlur();
                break;
            case 'A':
                isFileSave = '0';
                printf("Stretching Histogram.");
                stretchingHistogram();
                break;
            case 'B':
                isFileSave = '0';
                printf("Splot Filter.");
                do{
                    printf("Enter scope [0-7]: ");
                    scanf("%d %d", &scope);
                } while (scope < 0 || scope > 7);
                stretchingHistogram(scope);
                break;
            case 's':
                saveFile(defaultFileName);
                isFileSave = '1';
                printf("File save as %s \n", defaultFileName);
                break;
            case 'z':
                printf("Enter file name: ");
                scanf("%s", &name);
                char *tester = strchr(name, *".pgm");
                if (tester == NULL) strcat(name, ".pgm");

                if (access(name, F_OK) != -1){
                    puts("File with this name exists!");
                    puts("Do you want to overwrite the file? (y - Yes, n - No)");
                    char yesOrNot = 0;
                    scanf(" %c", &yesOrNot);
                    if (yesOrNot == 'y' || yesOrNot == 'Y'){
                        saveFile(name);
                        isFileSave = '1';
                        printf("File save as %s \n", name);
                        break;
                    }
                }
                isFileSave = '1';
                saveFile(name);
                break;    
            case 'x':
                if (isFileSave == '0'){
                    puts("You remember to save a file!");
                    puts("Exit this program now ? (y - Yes, n - No)");
                    char yesOrNot = 0;
                    scanf(" %c", &yesOrNot);
                    if (yesOrNot == 'y' || yesOrNot == 'Y'){
                        return;
                    }
                } else {
                    return;
                }
                break;
            default:
                break;
        }
        
        #ifdef _WIN32
            Sleep(1000);
        #else
            usleep(1000*1000);
        #endif
    }
}

void debugger(char fileName[]){

    #ifdef __linux__
    mkdir("pictures", 0777); 
    #else
            _mkdir("pictures");
    #endif

    // Negative
    constructor(fileName);
    convertToNegative();
    saveFile("pictures/negative.pgm");

    // thresholding 50%
    constructor(fileName);
    thresholding(50);
    saveFile("pictures/thresholding50.pgm");

    // thresholding 80%
    constructor(fileName);
    thresholding(80);
    saveFile("pictures/thresholding80.pgm");

    // halfBlackThresholding 50%
    constructor(fileName);
    halfBlackThresholding(50);
    saveFile("pictures/hBThresholding50.pgm");
    
    // halfBlackThresholding 80%
    constructor(fileName);
    halfBlackThresholding(80);
    saveFile("pictures/hBThresholding80.pgm");

    // halfWhiteThresholding 20%
    constructor(fileName);
    halfWhiteThresholding(20);
    saveFile("pictures/hWThresholding20.pgm");

    // halfWhiteThresholding 80%
    constructor(fileName);
    halfWhiteThresholding(80);
    saveFile("pictures/hWThresholding80.pgm");

    // gammaCorection 0.5
    constructor(fileName);
    gammaCorection(0.5);
    saveFile("pictures/gammaCorection0.5.pgm");

    // gammaCorection 1.5
    constructor(fileName);
    gammaCorection(1.5);
    saveFile("pictures/gammaCorection1.5.pgm");

    // levelCorection 20% B - 80% W
    constructor(fileName);
    levelCorection(20, 80);
    saveFile("pictures/levelCorection20W80B.pgm");

    // levelCorection 40% B - 60% W
    constructor(fileName);
    levelCorection(40, 60);
    saveFile("pictures/levelCorection40W60B.pgm");

    // conturing
    constructor(fileName);
    conturing(1);
    saveFile("pictures/conturing.pgm");

    // horizontalBlur
    constructor(fileName);
    horizontalBlur();
    saveFile("pictures/horizontalBlur.pgm");

    // verticalBlur
    constructor(fileName);
    verticalBlur();
    saveFile("pictures/verticalBlur.pgm");

    // stretchingHistogram
    constructor(fileName);
    stretchingHistogram();
    saveFile("pictures/stretchingHistogram.pgm");

    // splotFilter 1
    constructor(fileName);
    splotFilter(1);
    saveFile("pictures/splotFilter1.pgm");

    // splotFilter 2
    constructor(fileName);
    splotFilter(1);
    saveFile("pictures/splotFilter2.pgm");

    // splotFilter 3
    constructor(fileName);
    splotFilter(3);
    saveFile("pictures/splotFilter3.pgm");

    // splotFilter 4
    constructor(fileName);
    splotFilter(4);
    saveFile("pictures/splotFilter4.pgm");

    // splotFilter 5
    constructor(fileName);
    splotFilter(5);
    saveFile("pictures/splotFilter5.pgm");

    // splotFilter 6
    constructor(fileName);
    splotFilter(6);
    saveFile("pictures/splotFilter6.pgm");

    // splotFilter 7
    constructor(fileName);
    splotFilter(7);
    saveFile("pictures/splotFilter7.pgm");
}

