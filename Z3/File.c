#include "File.h"

File file;

void constructor(char fileName[]){
    
    char *tester = strchr(fileName, *".pgm");
    if (tester == NULL) strcat(fileName, ".pgm"); 

    FILE *fileReturn;
    fileReturn = fopen(fileName, "rw");
    if (fileReturn == NULL) throwError("File doesn't exist!", 1);
    readFile(fileReturn);
    fclose(fileReturn);
}

void readFile(FILE *fileOpen){
    
    char buffor[BUFFOR_SIZE];
    int comment;
    int end;
    File fileReturn = {0, 0, 0};
    
    if (fileOpen == NULL) throwError("File error.", 2);
    if (fgets(buffor, BUFFOR_SIZE, fileOpen) == NULL)
        throwError("Unexpected file error", 3.);

    if ( (buffor[0] != 'P') || (buffor[1] != '2')) {
        throwError("Bad file format!", 4);
    }

    do {
        if ((comment = fgetc(fileOpen)) == '#') {         
            if (fgets(buffor, BUFFOR_SIZE, fileOpen) == NULL) end = 1;                 
        } else {
            ungetc(comment, fileOpen);                   
        }                                      
    } while (comment == '#' && !end);


    if (fscanf(fileOpen,"%d %d %d",
         &fileReturn.width, &fileReturn.height, &fileReturn.shades) != 3) {
        throwError("Bad file settings.", 5);
    }

    for (int i = 0; i < fileReturn.height ; i++) {
        for (int j = 0; j < fileReturn.width ; j++) {
            if (fscanf(fileOpen, "%d", &(fileReturn.picture[i][j])) != 1) {
                throwError("Bad file resolution.", 6);
            }
        }
    }

    file = fileReturn;
}

void saveFile(char fileName[]){

    FILE *fileOpen;
    fileOpen = fopen(fileName, "w+");

    fprintf(fileOpen, "P2\n");
    fprintf(fileOpen, "%s\n", file.comment);
    fprintf(fileOpen, "%d %d\n", file.width, file.height);
    fprintf(fileOpen, "%d\n", file.shades);

    int t = 0;
    for (int i = 0; i < file.height; i++){ 
        for (int j = 0; j < file.width; j++){
            fprintf(fileOpen, "%3d ", file.picture[i][j]);
            t++;
            if (t == 20) {
                fprintf(fileOpen, "\n");
                t = 0;
            }
        }
    }
    fclose(fileOpen);
}

File convertToNegative(){ 

    for (int i = 0; i < file.height; i++){ 
        for (int j = 0; j < file.width; j++){
            file.picture[i][j] = file.shades - file.picture[i][j];
        }
    }

    addComment(" + negatyw");
    return file;
}

File thresholding(int scope){

    int prog = (file.shades*scope)/100;

    for (int i = 0; i < file.height; i++){ 
        for (int j = 0; j < file.width; j++){
            if (file.picture[i][j] > prog) file.picture[i][j] = file.shades;
            else if (file.picture[i][j] <= prog) file.picture[i][j] = 0;
        }
    }
    addComment(" + progowanie");
    return file;
}

File halfBlackThresholding(int scope){ 

    int prog = (file.shades*scope)/100;

    for (int i = 0; i < file.height; i++){ 
        for (int j = 0; j < file.width; j++){
            if (file.picture[i][j] <= prog) file.picture[i][j] = 0;
        }
    }
    addComment(" + pólprogowanie czerni");
    return file;
}

File halfWhiteThresholding(int scope){

    int prog = (file.shades*scope)/100;

    for (int i = 0; i < file.height; i++){ 
        for (int j = 0; j < file.width; j++){
            if (file.picture[i][j] > prog) file.picture[i][j] = file.shades;
        }
    }
    addComment(" + pólprogowanie bieli");
    return file;
}

File gammaCorection(float gamma){

    for (int i = 0; i < file.height; i++){ 
        for (int j = 0; j < file.width; j++){
            file.picture[i][j] = powf(file.picture[i][j], 1/gamma) /
             powf(file.shades, (1-gamma)/gamma);
        }
    }
    addComment(" + korekcje gamma");
    return file;
}

File levelCorection(int black, int white){

    int progBlack = (file.shades*black)/100; 
    int progWhite = (file.shades*white)/100;

    for (int i = 0; i < file.height; i++){ 
        for (int j = 0; j < file.width; j++){
            if (file.picture[i][j] <= progBlack) file.picture[i][j] = 0;
            else if (file.picture[i][j] < progWhite && file.picture[i][j] > progBlack) 
                file.picture[i][j] = (file.picture[i][j] - progBlack) * 
                    (file.shades/(progWhite - progBlack));
            else if (file.picture[i][j] >= progWhite) file.picture[i][j] = file.shades;
        }
    }
    addComment(" + zmiana poziomów");
    return file;
}

File conturing(int scope){

    int prog = (file.shades*scope)/100;
    float border;

    for (int i = 0; i < file.height; i++){ 
        for (int j = 0; j < file.width; j++){
            if (i+1 < file.height && j+1 < file.width){
                border = abs(file.picture[i + 1][j] - file.picture[i][j]) + 
                    abs(file.picture[i][j+1] - file.picture[i][j]);

                    if (border > prog) file.picture[i][j] = file.shades;
                    else file.picture[i][j] = 0;
            }
                
        }
    }

    addComment(" + konturowanie");
    return file;
}

File horizontalBlur(){

    for (int i = 0; i < file.height; i++){ 
        for (int j = 0; j < file.width; j++){
            if (j-1 > 0 && j+1 < file.width){
                 file.picture[i][j] = (file.picture[i][j+1] + file.picture[i][j]
                 + file.picture[i][j-1]) / 3;
            }
                
        }
    }

    addComment(" + rozmycie pionowe");
    return file;
}

File verticalBlur(){

    for (int i = 0; i < file.height; i++){ 
        for (int j = 0; j < file.width; j++){
            if (i-1 > 0 && i+1 < file.height){
                 file.picture[i][j] = (file.picture[i-1][j] + file.picture[i][j]
                 + file.picture[i+1][j]) / 3;
            }
                
        }
    }
    addComment(" + rozmycie poziome");
    return file;
}

File stretchingHistogram(){

    int min = file.picture[0][0];
    int max = file.picture[0][0];

    for (int i = 0; i < file.height; i++){ 
        for (int j = 0; j < file.width; j++){
            if (min > file.picture[i][j]) min = file.picture[i][j];
            if (max < file.picture[i][j]) max = file.picture[i][j];
        }
    }

    for (int i = 0; i < file.height; i++){ 
        for (int j = 0; j < file.width; j++){
            file.picture[i][j] = (file.picture[i][j] - min) * (file.shades/(max - min));    
        }
    }
    addComment(" + histogram");
    return file;
}

File splotFilter(int nr){

    File fileEdit = file;
    int *W;

    if (nr == 1){

        int A[3][3] = { {1, 1, 1},
                        {1, 1, 1},
                        {1, 1, 1}};
        W = *A;
    } else if (nr == 2){

        int A[3][3] = { {1, 1, 1},
                        {1, 2, 1},
                        {1, 1, 1}};
        W = *A;
    } else if (nr == 3){

        int A[3][3] = { { 0, 0, 0},
                        {-1, 0, 0},
                        { 0, 1, 0} };
        W = *A;
    } else if (nr == 4){
    
         int A[3][3] = { {-1, -1, -1},
                         { 0,  0,  0},
                         { 1,  1,  1} };
        W = *A;    
    } else if (nr == 5){

        int A[3][3] = { {-1, -2, -1},
                        { 0,  0,  0},
                        { 1,  2,  1} };
        W = *A;
    } else if (nr == 6){

        int A[3][3] = { { 1,  1,  1},
                        {-1, -2,  1},
                        {-1, -1,  1} };
        W = *A;
    } else if (nr == 7){
        
        int A[3][3] = { {-1, -1, -1},
                        {-1,  8, -1},
                        {-1, -1, -1} };
        W = *A;
    }


    int min;
    int max;
    
    for (int i = 0; i < file.height; i++){ 
        for (int j = 0; j < file.width; j++){
            if (i-1 >= 0 && i+1 <= file.height && j-1 >= 0 && j+1 <= file.width) {
                int prim = fileEdit.picture[i-1][j-1] * (*W) + fileEdit.picture[i][j-1] * (*W+1) + 
                    fileEdit.picture[i+1][j-1] * (*W+2) + fileEdit.picture[i-1][j] * (*W+3) + 
                    fileEdit.picture[i][j] * (*W+4) +  fileEdit.picture[i+1][j] * (*W+5) +
                    fileEdit.picture[i-1][j+1] * (*W+6) + fileEdit.picture[i][j+1] * (*W+7) +
                    fileEdit.picture[i+1][j+1] * (*W+8);

                if (i == 0 && j == 0){
                    min = prim;
                    max = prim;
                }
                if (min > prim) min = prim;
                if (max < prim) max = prim;
                file.picture[i][j] = prim;
            }
        }
    }

    for (int i = 1; i < file.height+1; i++){ 
        for (int j = 1; j < file.width+1; j++){
            float bis = ((file.picture[i][j] - min )* file.shades)/(max - min);
            file.picture[i][j] =  bis;
        }
    }
    addComment(" + splot");
}

void addComment(char string[]){

    if (file.comment[0] == '#'){
        strcat(file.comment, " ");
        strcat(file.comment, string);
    } else {
        strcpy(file.comment, "# ");
        strcat(file.comment, string);
    }
    
}

void throwError(char message[], int number){
    printf("[ERROR %d] %s \n", number, message);
    exit(number);
}
