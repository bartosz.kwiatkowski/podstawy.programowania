#include <stdio.h>
#include <stdlib.h>

#define MIN -10
#define MAX 10
#define FMIN 8
#define FMAX 14
#define SIZE_OF_PACKAGE 99

typedef enum{
    False = 0,
    True  = 1,
    None  = 2, 
} sign;

int number;
int numberAmount = 0;
int intersectionAmount = 0;
sign previousSign = None;
sign actuallySign = None;

int main (){
    
    while (True) {

        if (scanf("%d", &number) == 1) {
            
            numberAmount++;

            if (number == 99){
                break;
            }
            
            if (number >= MIN && number <= MAX) {
                
                if (numberAmount > 1) {
                    if (number == 0) actuallySign = previousSign;
                }
                
                if (number > 0)      actuallySign = 1;
                else if (number < 0) actuallySign = 0;

                if (numberAmount == 1) previousSign = actuallySign;
            

                if (((actuallySign ^ previousSign) == 1) && previousSign != None && actuallySign != None)
                    intersectionAmount++;

                if (numberAmount != 0) previousSign = actuallySign;
            }

            if (numberAmount == SIZE_OF_PACKAGE) {

                if ( FMIN > intersectionAmount || FMAX < intersectionAmount){
                    printf("Nieprawidlowy puls: %d \n",intersectionAmount);
                } else {
                    printf("Prawidlowy puls: %d \n",intersectionAmount);
                }
                        
                number = 100;
                numberAmount = 0;
                intersectionAmount = 0;
                previousSign = None;
                actuallySign = None;
            }
            
        }
    
    }
    
    return 0;
}
