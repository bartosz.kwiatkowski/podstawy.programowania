#include <stdio.h>

#define MIN -5
#define MAX 5

int main(){

    int i = 1;
    int number = 0;
    
    while (1) {
        printf("Podaj wartość %d. parametru: ", i); i++;
        scanf("%d", &number);

        if (number == 99) break;
        else if (number >= MIN && number <= MAX){
            printf("Aktualna wartosc parametru: %d\n", number);
        } else {
            puts("Bledna wartosc parametru!");
        }

    }

    return 0;
}