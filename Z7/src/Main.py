import numpy as np


# TODO
#   - priorytet na **
#   - ignore na []
#   - matchword %

def getLevensheinDistance(string1, string2):
    if string1 is None or string2 is None:
        return None

    stringLen1 = len(string1)
    stringLen2 = len(string2)
    matrix = np.zeros((stringLen1 + 1, stringLen2 + 1))
    cost = 0
    for i in range(stringLen1 + 1):
        matrix[i][0] = i

    for j in range(stringLen2 + 1):
        matrix[0][j] = j

    for i in range(1, stringLen1 + 1):
        for j in range(1, stringLen2 + 1):
            if string1[i - 1] is string2[j - 1]:
                cost = 0
            else:
                cost = 1

            matrix[i][j] = min((matrix[i][j - 1] + 1), (matrix[i - 1][j] + 1), (matrix[i - 1][j - 1] + cost))

    return matrix[stringLen1][stringLen2]


def separateWorlds(sentence):
    tmp = sentence[0].split()

    for index in range(len(tmp)):
        tmp[index] = tmp[index].lower()

    for i in range(len(tmp) - 1):
        for j in range(len(tmp[i]) - 1):
            ch = tmp[i][j]
            if not ((64 < ord(ch) < 91) or (96 < ord(ch) < 123)):
                if len(tmp[i]) is 1:
                    tmp.remove(tmp[i])
                else:
                    tmp[i] = tmp[i].replace(ch, '')
    return tmp


def removeComments(sentence):
    tmp = sentence
    for i in range(len(sentence)):
        tmp[i] = sentence[i][:sentence[i].find('[')] + sentence[i][sentence[i].find(']') + 1:]

    return tmp

def calculateSentenceMatch(sentence1, sentence2):
    matchWords = 0
    for word1 in sentence1:
        for word2 in sentence2:
            if getLevensheinDistance(word1, word2) < 3:
                matchWords = matchWords + 1

    return matchWords / len(sentence1) * 100


sent1 = ["Jest zdanie jakies"]
sent2 = ["Totalnie inne zdanie"]
print(sent1)
print(sent2)

sent2RemovedComments = removeComments(sent2)


print("Podobienstwo: ", calculateSentenceMatch(separateWorlds(sent1), separateWorlds(sent2RemovedComments)), "%")

