#!/bin/bash

cd $1
if [ ! -d "tmp" ]; then
    mkdir tmp
fi


for entry in ./*.JPEG
do
    var=$(echo $entry | sed -e "s/.JPEG/.jpeg/" |  tr -s ' ' '_' )
    convert $1/"$entry" -resize $2x$3 tmp/"$var"
done

for entry2 in ./*.PNG
do
    var=$(echo $entry2 | sed -e "s/.PNG/.png/" |  tr -s ' ' '_' )
    convert $1/"$entry2" -resize $2x$3 tmp/"$var"
done

tar -cvpf arch.tar.gz tmp
rm -rf tmp

