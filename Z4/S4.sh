#!/bin/bash

for entry in ./*.JPEG
do

    var=$(echo $entry | sed -e "s/.JPEG/.jpeg/")
    mv $entry $var
done

for entry in ./*.PNG
do

    var=$(echo $entry | sed -e "s/.PNG/.png/")
    mv $entry $var
done
